package com.example.balinasofttest.di;

import com.example.balinasofttest.RegistrationPresenter;
import com.example.balinasofttest.network.Api;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(MyAppl myAppl);

    void inject(RegistrationPresenter presenter);

    Api getApi();
}
