package com.example.balinasofttest.di;

import android.app.Application;
import android.content.Context;

public class MyAppl extends Application {
    private static MyAppl app;
    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        applicationComponent = DaggerApplicationComponent
                .builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        applicationComponent.inject(this);
    }

    public ApplicationComponent getComponent() {
        return applicationComponent;
    }

    public static MyAppl get(Context context) {
        return (MyAppl) context.getApplicationContext();
    }

    public static MyAppl app() {
        return app;
    }

}
