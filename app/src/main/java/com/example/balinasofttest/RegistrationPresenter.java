package com.example.balinasofttest;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.example.balinasofttest.di.MyAppl;
import com.example.balinasofttest.model.RegistrationModel;
import com.example.balinasofttest.network.Api;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@InjectViewState
public class RegistrationPresenter extends MvpPresenter<RegistrationView> {
    @Inject
    Api api;
    private RegistrationModel model = new RegistrationModel();
    static final int MIN_PASSWORD = 8;
    static final int MAX_PASSWORD = 15;
    static final int MIN_SUCCESS_RESPONSE_CODE = 200;
    static final int MAX_SUCCESS_RESPONSE_CODE = 300;
    static final int ERROR_RESPONSE_CODE = 400;
    private static final String POINT = ".";

    public RegistrationPresenter() {
        MyAppl.app().getComponent().inject(this);
    }

    void onSignUpClicked(String email, String password, String[] address, String[] domains) {
        if (checkInput(email, password, address, domains)) {
            getViewState().showProgressBar();
            model.setLogin(email.trim());
            model.setPassword(password.trim());
            signUpUser();
        }
    }

    private boolean checkInput(@NonNull String email, @NonNull String password, String[] address, String[] domains) {
        boolean checkEmptyEmail = true;
        boolean checkPassword = true;
        boolean checkMail = false;
        boolean checkDomain = false;
        if (password.isEmpty()) {
            getViewState().showEmptyPasswordMessage();
            checkPassword = false;
        } else if (password.length() < MIN_PASSWORD) {
            getViewState().showSmallPasswordMessage();
            checkPassword = false;
        } else if (password.length() > MAX_PASSWORD) {
            getViewState().showBigPasswordMessage();
            checkPassword = false;
        }
        if (email.isEmpty()) {
            getViewState().showEmptyEmailMessage();
            checkEmptyEmail = false;
        } else if (email.contains(Constants.EMAIL_SYMBOL) && email.contains(POINT)) {
            String post = email.substring(email.indexOf(Constants.EMAIL_SYMBOL) + 1, email.indexOf(POINT));
            String domain = email.substring(email.indexOf(POINT) + 1);
            for (int i = 0; i < address.length; i++) {
                if (post.equals(address[i])) {
                    checkMail = true;
                    break;
                }
            }
            for (int i = 0; i < domains.length; i++) {
                if (domain.equals(domains[i])) {
                    checkDomain = true;
                    break;
                }
            }
            if (!checkDomain) {
                getViewState().showDomainErrorMessage();
            }
            if (!checkMail) {
                getViewState().showPostErrorMessage();
            }
        } else if (!email.contains(Constants.EMAIL_SYMBOL) || !email.contains(POINT)) {
            getViewState().showEmailFormatErrorMessage();
        }
        return checkEmptyEmail && checkPassword && checkDomain && checkMail;
    }

    private void signUpUser() {
        api.signUp(model)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(x -> {
                    if (x.body() != null &&
                            x.code() >= MIN_SUCCESS_RESPONSE_CODE
                            && x.code() < MAX_SUCCESS_RESPONSE_CODE
                            && x.body().getData() != null) {
                        getViewState().showSuccessSignUpMessage(x.body().getData().getUserId());
                    } else if (x.code() == ERROR_RESPONSE_CODE) {
                        getViewState().showEmailIsAlreadyRegisteredMessage();
                    } else {
                        getViewState().showErrorSignUpMessage();
                    }
                }, x -> getViewState().showErrorSignUpMessage());
        getViewState().hideProgressBar();
    }

}
