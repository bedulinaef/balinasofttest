package com.example.balinasofttest;

import com.arellomobile.mvp.MvpView;

public interface RegistrationView extends MvpView {

    void showEmptyEmailMessage();

    void showEmptyPasswordMessage();

    void showSmallPasswordMessage();

    void showBigPasswordMessage();

    void showSuccessSignUpMessage(int userId);

    void showErrorSignUpMessage();

    void showPostErrorMessage();

    void showDomainErrorMessage();

    void showEmailFormatErrorMessage();

    void showEmailIsAlreadyRegisteredMessage();

    void showProgressBar();

    void hideProgressBar();
}
