package com.example.balinasofttest.network;

import com.example.balinasofttest.model.RegistrationModel;
import com.example.balinasofttest.model.ResponseModel;
import com.example.balinasofttest.model.SignUpData;

import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Api {

    @POST("/api/account/signup")
    Single<Response<ResponseModel<SignUpData>>> signUp(@Body RegistrationModel model);
}