package com.example.balinasofttest;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationActivity extends MvpAppCompatActivity implements RegistrationView {
    @InjectPresenter
    RegistrationPresenter mvpPresenter;
    private String[] emailAutocomplete;
    private ProgressDialog progressDialog;
    private String[] emails;
    private String[] address;
    private String[] domains;

    @BindView(R.id.tv_email)
    TextView tvEmail;

    @BindView(R.id.tv_password)
    TextView tvPassword;

    @BindView(R.id.et_email)
    AutoCompleteTextView etEmail;

    @BindView(R.id.et_password)
    EditText etPassword;

    @BindView(R.id.btn_sign_up)
    Button btnSignUp;

    @BindView(R.id.logo)
    ImageView logoImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        ButterKnife.bind(this);
        initProgressBar();
        setTypeface();
        address = getResources().getStringArray(R.array.address);
        domains = getResources().getStringArray(R.array.domain);
        emailAutocomplete = getResources().getStringArray(R.array.autocomplete);
        emails = new String[emailAutocomplete.length];
        Context context = this;
        etEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (etEmail.getText().toString().contains(Constants.EMAIL_SYMBOL)) {
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, emails);
                    etEmail.setAdapter(adapter);
                } else {
                    for (int i = 0; i < emailAutocomplete.length; i++) {
                        emails[i] = etEmail.getText().toString() + emailAutocomplete[i];
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, emails);
                    etEmail.setAdapter(adapter);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public void showEmptyEmailMessage() {
        etEmail.setError(getResources().getString(R.string.empty_email));
    }

    @Override
    public void showEmptyPasswordMessage() {
        etPassword.setError(getResources().getString(R.string.empty_password));
    }

    @Override
    public void showSmallPasswordMessage() {
        etPassword.setError(getResources().getString(R.string.small_password_error));
    }

    @Override
    public void showBigPasswordMessage() {
        etPassword.setError(getResources().getString(R.string.big_password_error));
    }

    @Override
    public void showSuccessSignUpMessage(int userId) {
        Toast.makeText(getApplicationContext(), String.valueOf(userId), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showErrorSignUpMessage() {
        Toast.makeText(this, getResources().getString(R.string.network_error), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showPostErrorMessage() {
        etEmail.setError(getResources().getString(R.string.address_error));
    }

    @Override
    public void showDomainErrorMessage() {
        etEmail.setError(getResources().getString(R.string.address_error));
    }

    @Override
    public void showEmailFormatErrorMessage() {
        etEmail.setError(getResources().getString(R.string.email_format_error));
    }

    @Override
    public void showEmailIsAlreadyRegisteredMessage() {
        Toast.makeText(this, getResources().getString(R.string.email_is_already_registered), Toast.LENGTH_LONG).show();
    }

    @Override
    public void showProgressBar() {
        progressDialog.show();
    }

    @Override
    public void hideProgressBar() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.btn_sign_up)
    void onSignUpClick() {
        mvpPresenter.onSignUpClicked(etEmail.getText().toString(), etPassword.getText().toString(), address, domains);
    }

    private void setTypeface() {
        Typeface type = Typeface.createFromAsset(getAssets(), "Helvetica_Neue.ttf");
        etEmail.setTypeface(type);
        etPassword.setTypeface(type);
        tvEmail.setTypeface(type);
        tvPassword.setTypeface(type);
        btnSignUp.setTypeface(type);

    }

    private void initProgressBar() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(getString(R.string.wait_for_answer));
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
    }

}
